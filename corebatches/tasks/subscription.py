import logging
from datetime import datetime

from flask import g
from corebatches.tasks import with_app_context
from task.controller.subscription import send_sms
from task.model import db


@with_app_context
def Sms():
    logger = logging.getLogger(__name__)
    logger.info('Getting subscriptions to send ---------------')

    dt_subscription = datetime.now().replace(minute=0, second=0, microsecond=0)
    subscriptions = g.tran.query(db.Subscription).filter(db.Subscription.is_sent == False,
                                                         db.Subscription.datetime == dt_subscription).all()

    logger.info(f'subscriptions length: {len(subscriptions)}')

    for ind, subscription in enumerate(subscriptions):
        try:
            send_sms(subscription.supplier.phone, subscription.message)

            subscription.is_sent = True
            g.tran.add(subscription)
        except Exception as e:
            g.tran.rollback()
            logger.error(e)
