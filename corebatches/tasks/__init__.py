import logging
from flask import g

from task.AppFactory import create_app

__author__ = 'Kuba'


def with_app_context(func):
    def wrapper(session, *args, **kwargs):
        app = create_app('task', logger_name=None)
        with app.app_context():
            g.tran = session()
            g.test = False
            g.debug = False
            g.logger = logging.getLogger(func.__name__)
            try:
                func()
                g.tran.commit()
            except Exception as e:
                g.tran.rollback()
                g.logger.exception(e)
            finally:
                session.remove()

    return wrapper
