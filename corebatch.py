import logging.handlers
import os
import signal
import sys
import time
from datetime import datetime

from apscheduler.executors.pool import ThreadPoolExecutor
from apscheduler.jobstores.memory import MemoryJobStore
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.blocking import BlockingScheduler

from task import SessionFactory
from task.config import config
from task.model import db

__author__ = 'Kuba'

logging.basicConfig(level=logging.DEBUG,
                    format="%(threadName)s %(asctime)s %(name)-12s %(message)s",
                    datefmt="%d-%m-%y %H:%M")
path = 'log'
if not os.path.exists(path):
    os.makedirs(path)
filename = os.path.join(path, "corebatch.log")
daily = logging.handlers.WatchedFileHandler(filename, encoding="utf-8")
fmt = logging.Formatter('%(threadName)s %(asctime)s %(name)-12s %(message)s')
daily.setFormatter(fmt)

if config().get('system', 'debug') == "False":
    daily.setLevel(logging.INFO)
else:
    daily.setLevel(logging.DEBUG)
logging.getLogger().addHandler(daily)


class Scheduler:
    should_stop = False
    scheduler = None

    def __init__(self):
        catchSignals = [
            signal.SIGINT,
            signal.SIGQUIT,
            signal.SIGTERM,
        ]
        for signum in catchSignals:
            signal.signal(signum, self.stop)

    def run(self):
        job_stores = {
            'default': MemoryJobStore()
        }
        executors = {
            'default': ThreadPoolExecutor(20),
        }
        if sys.platform.lower() == 'win32':
            self.scheduler = BlockingScheduler(jobstores=job_stores, executors=executors)
        else:
            self.scheduler = BackgroundScheduler(jobstores=job_stores, executors=executors)

        session = SessionFactory.get_session(app_name='CoreBatches')
        try:
            jobs = session.query(db.ScheduleJob).filter(db.ScheduleJob.is_enabled == True).all()
            for job in jobs:
                try:
                    names = job.name.split('.')

                    if len(names) != 2:
                        continue
                    m = __import__('corebatches.tasks.' + names[0])
                    m = getattr(m, 'tasks')
                    m = getattr(m, names[0])
                    m = getattr(m, names[1])

                    if job.interval == 'hours':
                        now = datetime.now()
                        next_run_time = now.replace(hour=now.hour + 1, minute=0, second=0, microsecond=0)
                        self.scheduler.add_job(m, trigger='interval', hours=int(job.interval_value),
                                               args=[session], next_run_time=next_run_time, name=job.name)
                except Exception as e:
                    logging.exception(e)
        finally:
            session.remove()
        self.scheduler.start()
        while self.scheduler.running and not self.should_stop:
            time.sleep(0.5)

    def stop(self):
        self.should_stop = True
        self.scheduler.shutdown(True)


if __name__ == "__main__":
    daemon = Scheduler()
    try:
        daemon.run()
    except KeyboardInterrupt:
        daemon.stop()
