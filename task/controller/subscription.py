from datetime import datetime, timedelta


def send_sms(phone, message):
    # TODO sending sms
    pass


def get_subscription_datetime(district_utc, subscription_hour):
    tomorrow = datetime.now().date() + timedelta(days=1)
    utc = datetime.utcnow()
    diff_with_utc = int((datetime.now() - utc).total_seconds() / 3600)
    hour = subscription_hour + (diff_with_utc - district_utc)
    time = datetime.strptime(str(hour), '%H').time()
    subscription_datetime = datetime.combine(tomorrow, time)
    return subscription_datetime
