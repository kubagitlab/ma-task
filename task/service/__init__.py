import logging

from flask import g

from task import error_codes
from task.exceptions import CoreException

__author__ = 'Kuba'


def call(service_name, bag):
    module = service_name.split('.')
    m = __import__('task.service.' + module[0])
    m = getattr(m, 'service')
    m = getattr(m, module[0])
    if type(m).__name__ != 'module':
        raise CoreException(error_codes.SYSTEM_FAILURE)
    m = getattr(m, module[1])
    logger = logging.getLogger(service_name)
    logger.setLevel(logging.DEBUG if hasattr(g, 'debug') and g.debug else logging.INFO)
    g.logger = logging.LoggerAdapter(logger, extra={'ip': bag.get('__session__', {}).get('ip') or 'none'})
    return m(bag)
