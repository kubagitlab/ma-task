from datetime import datetime

from flask import g

from task.controller.subscription import get_subscription_datetime
from task.main_utils import get_config
from task.model import db

__author__ = 'Kuba'


def send_subscription(data):
    subscription_hour = get_config('subscription_hour', 10)
    for district_id in data:
        suppliers = g.tran.query(db.Supplier).filter(db.Supplier.district_id == district_id,
                                                     db.Supplier.subscription_cancelled != True).all()
        if len(suppliers) == 0:
            continue
        utc = data[district_id]
        subscription_datetime = get_subscription_datetime(utc, subscription_hour)
        for supplier in suppliers:
            existing_subscr = g.tran.query(db.Subscription).filter(db.Subscription.supplier_id == supplier.id,
                                                                   db.Subscription.is_sent == False).first()
            if existing_subscr:
                continue
            subscription = db.Subscription()
            subscription.supplier_id = supplier.id
            subscription.datetime = subscription_datetime
            subscription.is_sent = False
            subscription.message = 'Текст рассылки'
            g.tran.add(subscription)
    g.tran.commit()
