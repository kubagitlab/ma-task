import traceback
from inspect import isclass
import logging
from factory.alchemy import SQLAlchemyModelFactory
from sqlalchemy.exc import IntegrityError

__author__ = 'Kuba'


def create_all(cls, session, test=False):
    classes = sorted([x for x in dir(cls) if isclass(getattr(cls, x))])
    for c in classes:
        m = getattr(cls, c)
        b = m.__bases__
        if len(b) > 0 and b[0].__name__ == FixtureBase.__name__:
            t = m(session)
            t.create(test)
            name = t.model_for.__table__.name
            try:
                session.bind.execute("SELECT setval('{0}_id_seq', (SELECT max(id) FROM {0}))".format(name))
            except:
                pass
        if issubclass(m, SQLAlchemyModelFactory) and m.__name__ != SQLAlchemyModelFactory.__name__:
            m._meta.abstract = False
            m._meta.sqlalchemy_session = session
            try:
                m.create()
                session.commit()
                name = m._meta.model.__table__.name
                session.bind.execute("SELECT setval('{0}_id_seq', (SELECT max(id) FROM {0}))".format(name))
            except IntegrityError as e:
                session.rollback()
                if getattr(e.orig, 'pgcode', None) != '23505':
                    logging.info(e.message)
                    traceback.print_exc()


class FixtureBase:
    model_for = None
    session = None
    data = []
    keys = []

    def __getitem__(self, item, col='id'):
        return getattr(self, item)._declarations[col]

    def __init__(self, model_for=None, session=None):
        self.model_for = model_for
        self.session = session

    def create(self, test=False):
        for d in self.data:
            sql = self.session.query(self.model_for)
            if self.keys:
                for k in self.keys:
                    sql = sql.filter(getattr(self.model_for, k) == d[k])
            else:
                sql = sql.filter_by(**d)

            model = sql.first()
            if model and (d.get('update') or test):
                logging.info('Updating {}'.format(d))
                for key in d.keys():
                    if hasattr(model, key):
                        setattr(model, key, d[key])
            if not model:
                model = self.model_for()
                logging.info('Inserting {}'.format(d))
                for key in d.keys():
                    if hasattr(model, key):
                        setattr(model, key, d[key])
                self.session.add(model)
            try:
                self.session.commit()
            except IntegrityError as e:
                self.session.rollback()
                if getattr(e.orig, 'pgcode', None) != '23505':
                    logging.info(e.message)

        classes = sorted([x for x in dir(self) if isclass(getattr(self, x))])
        for c in classes:
            m = getattr(self, c)
            if issubclass(m, SQLAlchemyModelFactory) and m.__name__ != SQLAlchemyModelFactory.__name__:
                m._meta.abstract = False
                m._meta.model = self.model_for
                m._meta.sqlalchemy_session = self.session
                try:
                    m.create()
                    self.session.commit()
                except IntegrityError as e:
                    self.session.rollback()
                    if getattr(e.orig, 'pgcode', None) != '23505':
                        logging.info(e.message)
