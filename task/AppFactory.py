import logging
import logging.handlers
import os

from task.config import config
from flask import Flask

__author__ = 'Kuba'


def create_app(name, logger_name='daily.log', test=False):
    if not test:
        logging.basicConfig(level=logging.INFO,
                            format="%(threadName)s %(asctime)s %(name)-12s %(message)s",
                            datefmt="%d-%m-%y %H:%M")
        if not os.path.exists('log/'):
            os.makedirs('log')
        if logger_name:
            daily = logging.handlers.TimedRotatingFileHandler("log/" + logger_name, when="midnight", interval=1,
                                                              backupCount=15, encoding="utf-8")
            fmt = logging.Formatter('%(asctime)s %(name)-12s %(message)s')
            daily.setFormatter(fmt)
            logging.getLogger().addHandler(daily)

            if config().get('system', 'debug') == "False":
                daily.setLevel(logging.INFO)
            else:
                daily.setLevel(logging.DEBUG)
    logging.info(u'Starting server {}'.format(name))
    if test:
        app = Flask(name)
    else:
        app = Flask(name, static_folder=None)
    return app
