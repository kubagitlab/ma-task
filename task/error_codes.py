__author__ = 'Kuba'

WRONG_CONFIRM_CODE = -100
GENERIC_ERROR = -2
SYSTEM_FAILURE = -1
OK = 0
NOT_ENOUGH_PARAMETERS = 2
RECORD_NOT_FOUND = 31

ERRORS = {
    SYSTEM_FAILURE: u"Ошибка системы",
    OK: "Success",
    NOT_ENOUGH_PARAMETERS: u"Недостаточно параметра {0}",
    RECORD_NOT_FOUND: u'Запись не найдена',

}
