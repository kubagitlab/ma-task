from configparser import ConfigParser, NoOptionError, NoSectionError

__author__ = 'Kuba'
__config__ = 'config.ini'


class config(ConfigParser):
    def __init__(self):
        ConfigParser.__init__(self)
        self.load()

    def versionObsolete(self):
        with open(__config__) as f:
            f.readline()
            line = f.readline()
            version = line.split('=')[1].strip()
            return not self.get('system', 'version') or self.get('system', 'version') != version

    def load(self):
        self.read(__config__)

    def __getitem__(self, item):
        try:
            if isinstance(item, tuple) and item[1] is not None:
                return self.get(item[1].__name__, item[0])
            else:
                return self.get('system', item)
        except NoOptionError or NoSectionError:
            return ''
