import decimal
import json
from datetime import datetime, time, date, timedelta
from sqlalchemy.ext.declarative import DeclarativeMeta
from flask.wrappers import Response
from task.config import config

__author__ = 'Kuba'

from task.error_codes import ERRORS


def orm_to_json(orm):
    if isinstance(orm, list):
        ret = []
        for o in orm:
            ret.append(orm_to_json(o))
        return ret
    else:
        d = {}
        for key in dir(orm):
            if key.startswith('_'):
                continue
            if key.startswith('metadata'):
                continue
            value = getattr(orm, key)
            if hasattr(value, '__call__'):
                continue
            d[key] = getattr(orm, key)
        return d


def json_to_orm(json_, orm):
    """
    Merge in items in the values dict into our object if it's one of our columns
    """
    if hasattr(orm, '__table__'):
        for c in orm.__table__.columns:
            if c.name in json_:
                setattr(orm, c.name, json_[c.name])
    else:
        for c in orm._asdict().keys():
            if c in json_:
                setattr(orm, c, json_[c])


class JSONEncoderCore(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            r = str(o)[:19]
            return r
        elif isinstance(o, date):
            return str(o)
        elif isinstance(o, time):
            r = str(o)
            return r
        elif isinstance(o, decimal.Decimal):
            return float(o)
        elif isinstance(o, timedelta):
            return o.total_seconds()
        elif isinstance(o.__class__, DeclarativeMeta):
            return orm_to_json(o)
        else:
            return super(JSONEncoderCore, self).default(o)


def get_config(name, default_value):
    return int(config().get('system', name)) if config().has_option('system', name) else default_value


def get_response(p_content, p_type='json'):
    if p_type == 'sms':
        return p_content['message']
    else:
        if 'result' not in p_content:
            p_content.update({'result': 0})
        return Response(json.dumps(p_content, cls=JSONEncoderCore), mimetype='application/json; charset=utf-8')


def get_error(code, p_type='json', extra=None):
    if p_type == 'sms':
        return ERRORS[code]
    else:
        if code in ERRORS:
            return Response(json.dumps({'result': code, 'message': ERRORS.get(code, '').format(extra), 'extra': extra},
                                       cls=JSONEncoderCore), mimetype='application/json; charset=utf-8')
        return Response(json.dumps({'result': code, 'message': 'Unknown error', 'extra': extra},
                                   cls=JSONEncoderCore), mimetype='application/json; charset=utf-8')
