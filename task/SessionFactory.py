from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.pool import NullPool

from task.config import config

__author__ = 'Kuba'


def build_session(db=None, db_string='db_string', app_name='', url=None, pool_size=None):
    global DB_SESSION
    global DB_ENGINE
    global TEST
    if "DB_SESSION" not in globals():
        DB_ENGINE = {}
        DB_SESSION = {}

    if db_string in DB_SESSION:
        return
    if not url:
        url = config().get('system', db_string)
    if config().has_option('system', 'pool_size'):
        pool_size = int(config().get('system', 'pool_size'))
    if pool_size:
        DB_ENGINE[db_string] = create_engine(url, pool_size=pool_size, connect_args={'application_name': app_name})
    else:
        DB_ENGINE[db_string] = create_engine(url, poolclass=NullPool, connect_args={'application_name': app_name})

    DB_SESSION[db_string] = scoped_session(sessionmaker(bind=DB_ENGINE[db_string], autoflush=True))
    if db:
        db.Base.metadata.create_all(DB_ENGINE[db_string])


def get_engine(db=None, db_string='db_string', app_name='', url=None):
    build_session(db, db_string, app_name, url)
    return DB_ENGINE[db_string]


def get_session(db=None, db_string='db_string', app_name='', url=None):
    build_session(db, db_string, app_name, url)
    return DB_SESSION[db_string]


def dispose():
    for k in DB_SESSION.iterkeys():
        DB_SESSION[k].close()
    for k in DB_ENGINE.iterkeys():
        DB_ENGINE[k].dispose()
    del globals()['DB_ENGINE']
    del globals()['DB_SESSION']
