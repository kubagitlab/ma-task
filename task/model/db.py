from sqlalchemy import Column, Integer, String, ForeignKey, text, BIGINT, DateTime, Boolean, \
    TEXT
from sqlalchemy.orm import relationship

from task.model import Base

__author__ = 'Kuba'


class District(Base):
    __tablename__ = 'district'
    id = Column(Integer, primary_key=True)
    name = Column(String(255, 'Cyrillic_General_CI_AS'), nullable=False, comment="Название области")


class Supplier(Base):
    __tablename__ = 'supplier'
    name = Column(String(255), nullable=True)
    contact_person = Column(String(255), nullable=True)
    inn = Column(String(15), nullable=True)
    storage_address = Column(String(255))
    phone = Column(String(255))
    id = Column(Integer, primary_key=True)
    subscription_cancelled = Column(Boolean, nullable=True, comment="Отписан ли от рассылки")
    subscription_admin = Column(Boolean, nullable=True, comment="Отписан ли от рассылки админом")
    district_id = Column(ForeignKey('district.id'), nullable=True, comment="id области")
    district = relationship('District')
    # area_id = Column(ForeignKey('area.id'), nullable=True, comment="id района")
    # area = relationship('Area')
    # manager_id = Column(ForeignKey('user.id'), nullable=True)
    # manager = relationship('User')
    # land_crop = relationship('LandCrop', secondary=supplier_land_crop, backref=backref('suppliers'))
    # landuser = Column(String(255, 'Cyrillic_General_CI_AS'), nullable=True)


class Subscription(Base):
    __tablename__ = "subscription"
    id = Column(BIGINT, primary_key=True)
    supplier_id = Column(ForeignKey('supplier.id'), nullable=False, comment="id поставщика")
    supplier = relationship('Supplier')
    datetime = Column(DateTime, nullable=False)
    message = Column(TEXT, nullable=False)
    is_sent = Column(Boolean, nullable=True, comment="Отправлен ли рассылка")
    created_datetime = Column(DateTime, server_default=text('current_timestamp'))


class ScheduleJob(Base):
    __tablename__ = "schedule_job"
    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    interval = Column(String(50))
    interval_value = Column(String(50))
    is_enabled = Column(Boolean)
