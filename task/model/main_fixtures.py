from factory.alchemy import SQLAlchemyModelFactory
from task.fixtures import FixtureBase
from task.model import db


class ScheduleJobs(FixtureBase):
    def __init__(self, session):
        FixtureBase.__init__(self, db.ScheduleJob, session)

    class EveryHour(SQLAlchemyModelFactory):
        class Meta: model = db.ScheduleJob

        id = 1
        name = 'subscription.Sms'
        interval = 'hours'
        interval_value = '1'
        is_enabled = True

    class Every5Second(SQLAlchemyModelFactory):
        class Meta: model = db.ScheduleJob

        id = 2
        name = 'subscription.Sms'
        interval = 'seconds'
        interval_value = '5'
        is_enabled = False


class Districts(FixtureBase):
    def __init__(self, session):
        FixtureBase.__init__(self, db.District, session)

    class Voronej(SQLAlchemyModelFactory):
        class Meta: model = db.District

        id = 1
        name = 'Воронежская '

    class Amur(SQLAlchemyModelFactory):
        class Meta: model = db.District

        id = 2
        name = 'Амурская'


class Suppliers(FixtureBase):
    def __init__(self, session):
        FixtureBase.__init__(self, db.Supplier, session)

    class Romashka(SQLAlchemyModelFactory):
        class Meta: model = db.Supplier

        id = 1
        name = 'ООО «Ромашка»'
        storage_address = 'Адрес'
        phone = '+7 000 000 00 00'
        subscription_cancelled = False
        subscription_admin = False
        district_id = 1

    class Pupuchik(SQLAlchemyModelFactory):
        class Meta: model = db.Supplier

        id = 2
        name = 'ООО «Пупучик»'
        storage_address = 'Адрес'
        phone = '+7 000 000 00 11'
        subscription_cancelled = False
        subscription_admin = False
        district_id = 2

    class Romashka2(SQLAlchemyModelFactory):
        class Meta: model = db.Supplier

        id = 3
        name = 'ООО «Ромашка2»'
        storage_address = 'Адрес'
        phone = '+7 000 000 00 02'
        subscription_cancelled = False
        subscription_admin = False
        district_id = 1
