import json
import logging

from flask import Blueprint, request
from task import service
from task.main_utils import get_response

init = Blueprint('init', __name__)


@init.route('/', defaults={'path': ''}, methods=['GET', 'POST'])
@init.route('/<path:path>', methods=['GET', 'POST'])
def index(path):
    service_name = path.replace('/', '.')
    bag = {}
    if request.method == 'POST':
        bag = json.loads(request.data)
    elif request.method == 'GET':
        for key in request.args:
            bag[key] = request.args[key]
            if key == '__session__':
                bag[key] = json.loads(request.args[key])

    service.call(service_name, bag)
    response = get_response(bag, request.args.get('response_type', 'json'))
    msg = 'Request:{0} - Response data: {1}'
    data = response.data
    logging.debug(msg.format(path, data))

    return response
