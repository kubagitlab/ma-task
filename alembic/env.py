from __future__ import with_statement

import os
import re
import sys
from logging.config import fileConfig

from alembic import context
from sqlalchemy import create_engine, pool
from sqlalchemy.dialects.postgresql import DropEnumType
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.schema import CreateTable, DropTable, CreateIndex, DropSequence, DropIndex

sys.path.insert(0, os.path.realpath(os.path.join(os.path.dirname(__file__), '..')))
from task.model.db import Base
from task.config import config as Config

patches = (
    (CreateTable, 'visit_create_table', "^\s*CREATE TABLE", "CREATE TABLE IF NOT EXISTS"),
    (CreateIndex, 'visit_create_index', "^\s*CREATE INDEX", "CREATE INDEX IF NOT EXISTS"),
    (DropTable, 'visit_drop_table', "^\s*DROP TABLE", "DROP TABLE IF EXISTS"),
    (DropSequence, 'visit_drop_sequence', "^\s*DROP SEQUENCE", "DROP SEQUENCE IF EXISTS"),
    (DropIndex, 'visit_drop_index', "^\s*DROP INDEX", "DROP INDEX IF EXISTS"),
    (DropEnumType, 'visit_drop_enum_type', "^\s*DROP TYPE", "DROP TYPE IF EXISTS")
)


def create_patch(visitor, method, re_from, re_to, if_always=False):
    @compiles(visitor)
    def _if_exists_(element, compiler, **kw):
        output = getattr(compiler, method)(element, **kw)
        if if_always or element.element.info.get('ifexists'):
            output = re.sub(re_from, re_to, output, re.S)
        return output

    return _if_exists_


def enable_patches(if_always=False):
    for patch in patches:
        create_patch(*patch, if_always=if_always)


enable_patches(True)

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.

config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if not context.get_x_argument(as_dictionary=True).get('silent'):
    fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = Base.metadata


# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.

def include_symbol(tablename, schema):
    _digits = re.compile('\d')
    return not (bool(_digits.search(tablename))
                or tablename.startswith('adj')
                or tablename.startswith('avt')
                or 'ikb' in tablename
                or '_history' in tablename) or tablename == 'balance_history'


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = Config().get('system', 'db_string')
    db_path = context.get_x_argument(as_dictionary=True).get('dbPath')
    if db_path:
        url = db_path

    context.configure(url=url, target_metadata=target_metadata, literal_binds=True, include_symbol=include_symbol)

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    url = Config().get('system', 'db_string')
    db_path = context.get_x_argument(as_dictionary=True).get('dbPath')
    if db_path:
        url = db_path

    connectable = create_engine(url, poolclass=pool.NullPool)

    with connectable.connect() as connection:
        context.configure(connection=connection, target_metadata=target_metadata, include_symbol=include_symbol,
                          compare_type=True)

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
