import logging
import traceback
from time import time

from flask import g, request
from sqlalchemy.orm.exc import NoResultFound

import task
from task import AppFactory, fixtures
from task import SessionFactory
from task.config import config
from task.error_codes import RECORD_NOT_FOUND, NOT_ENOUGH_PARAMETERS, SYSTEM_FAILURE
from task.exceptions import CoreException
from task.main_utils import get_error
from task.model import db
from task.pages import init

app = AppFactory.create_app(task.__name__)
msg = 'Requested: {0} - Client data: {1}'


@app.before_request
def before():
    g.time = time()
    g.test = config().get('system', 'mode') == 'TEST'
    g.debug = config().get('system', 'debug') == 'True'
    logger = logging.getLogger('core')
    logger.setLevel(logging.DEBUG if g.debug else logging.INFO)
    g.logger = logging.LoggerAdapter(logger, None)

    session = SessionFactory.get_session(app_name='Ma_Task')
    g.tran = session()


@app.teardown_request
def teardown(exception):
    try:
        if not exception:
            if not g.tran:
                g.tran.commit()
        else:
            g.logger.warning(exception.message)
            g.logger.error(traceback.format_exc())
            if not g.tran:
                g.tran.rollback()
            if isinstance(exception, CoreException):
                return error(exception)
            elif isinstance(exception, KeyError) or isinstance(exception, ValueError):
                return unknown(exception.message, RECORD_NOT_FOUND)
            return unknown(exception)
    finally:
        g.user = None
        g.tran = None
        SessionFactory.get_session().remove()


@app.errorhandler(Exception)
@app.errorhandler(CoreException)
@app.errorhandler(500)
def generic_error_handler(exception):
    try:
        data = request.data

        if hasattr(g, 'logger'):
            g.logger.info(msg.format(request.url, data))
            g.logger.exception(exception.message)
            g.logger.exception(traceback.format_exc())
        if hasattr(g, 'tran'):
            g.tran.rollback()
        if isinstance(exception, CoreException):
            return error(exception)
        elif isinstance(exception, KeyError):
            return unknown(exception.message, NOT_ENOUGH_PARAMETERS)
        elif isinstance(exception, NoResultFound):
            return unknown('', code=RECORD_NOT_FOUND)
        return unknown('')
    finally:
        g.user = None
        g.tran = None
        SessionFactory.get_session().remove()


def error(e):
    return get_error(e.code, request.args.get('response_type', 'json'), e.message)


def unknown(message, code=SYSTEM_FAILURE):
    return get_error(code, request.args.get('response_type', 'json'), message)


app.register_error_handler(CoreException, teardown)
app.register_blueprint(init)


def create_fixtures():
    # insert initial fixtures
    session = SessionFactory.get_session()
    try:
        fixtures.create_all(db, session())
        if config().has_option('system', 'fixtures'):
            for f in config().get('system', 'fixtures').split(','):
                m = __import__('task.model.' + f)
                m = getattr(m, 'model')
                m = getattr(m, f)

                fixtures.create_all(m, session)
    finally:
        session.remove()


if __name__ == "__main__":
    create_fixtures()

    app.run(host='0.0.0.0', port=7100, threaded=True, debug=True)
